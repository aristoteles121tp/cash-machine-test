## Cash machine test

Este es un proyecto hecho con ```Nest js.```

## Installation de módulos

Antes de correr la app se deben instalar las dependencias con el siguiente comando en la raíz del proyecto
```bash
$ npm install
```

## Configuración

En la raíz del proyecto se debe crear un archivo ```.env``` el cual contandrá la configuración de la base de datos, el puerto por el cual se levantará el servidor, etc. Hay un archivo ```.env.example``` en la raíz del proyecto el cual se puede usar como base para llenar los datos correspondientes.

## Base de datos

El proyecto se probo con una base de datos de postgres.

### Configuración de la base de datos

1. Para crear la base de datos es necesario ajustar las siguientes variables en el archivo ```.env```;

```bash
DB_MIGRATIONS=false
DB_DROP=true
DB_SYNCHRONIZE=true
```

Una vez modificadas se debe levantar el proyecto por primera vez con el siguiente comando:
```
$ npm run start
```

Una vez que se haya levantado la aplicación, la base de datos estara creada correctamente y se debe dejar de ejecutar la aplicación para continuar con el siguiente paso.

2. La siguiente configuración es la que debe ir por default en el archivo ```.env```para que se ejecuten las migraciones cada vez que se redeploye la aplicación:

```
DB_MIGRATIONS=true
DB_DROP=false
DB_SYNCHRONIZE=false
```

## Levantar la app

```bash
# development
$ npm run start

# production mode
$ npm run start:prod
```

## Pruebas

```bash
# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
