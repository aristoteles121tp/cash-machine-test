import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { join } from 'path';

export const getTypeOrmOptions = (): TypeOrmModuleOptions => {
  return {
    type: process.env.DB_TYPE as any,
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10) || 5433,
    username: process.env.DB_USER_NAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: [join(__dirname, '..', 'postgres', 'entities', '*{.ts,.js}')],
    synchronize: process.env.DB_SYNCHRONIZE === 'true',
    dropSchema: process.env.DB_DROP === 'true',
    logging: process.env.DB_LOGGING === 'true',
    autoLoadEntities: true,
    migrationsRun: process.env.DB_MIGRATIONS === 'true',
    migrations: [join(__dirname, '..', 'postgres', 'migrations', '*{.ts,.js}')],
  } as any;
};
