import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  BeforeInsert,
} from 'typeorm';
import { CardType } from './CardType';
import { Account } from './/Account';
import { HashTools } from '../../../core/utils/hash-tools';

@Entity()
export class Card {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    length: 50,
    name: 'status',
    nullable: false,
  })
  status: string;

  @Column('boolean', {
    name: 'is_active',
    nullable: false,
  })
  isActive: boolean;

  @Column('character varying', {
    length: 20,
    name: 'number',
    nullable: false,
    unique: true,
  })
  number: string;

  @Column('character varying', {
    length: 5,
    name: 'cvc',
    nullable: false,
  })
  cvc: string;

  @Column('character varying', {
    length: 150,
    name: 'nip',
    nullable: false,
  })
  nip: string;

  @Column('timestamp without time zone', {
    name: 'expiry_date',
    nullable: false,
  })
  expiryDate: Date;

  @BeforeInsert()
  async hashPassword() {
    this.nip = await new HashTools().hashText(this.nip);
  }

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToOne(() => Account, (account) => account.cards)
  account?: Account;

  @ManyToOne(() => CardType, (cardType) => cardType.cards)
  cardType?: CardType;
}
