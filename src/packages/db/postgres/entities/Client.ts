import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { Account } from './Account';

@Entity()
export class Client {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    length: 50,
    name: 'first_name',
    nullable: false,
  })
  firstName: string;

  @Column('character varying', {
    length: 50,
    name: 'last_name',
    nullable: true,
  })
  lastName: string;

  @Column('character varying', {
    length: 50,
    name: 'name',
    nullable: false,
  })
  name: string;

  @Column('character varying', {
    length: 150,
    name: 'address',
    nullable: true,
  })
  address?: string;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @OneToMany(() => Account, (account) => account.client)
  accounts?: Account[];
}
