import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Client } from './Client';
import { AccountType } from './AccountType';
import { Card } from './Card';
import { Transaction } from './Transaction';

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    length: 15,
    name: 'number',
    nullable: false,
    unique: true,
  })
  number: string;

  @Column('character varying', {
    length: 25,
    name: 'interbank_key',
    nullable: false,
    unique: true,
  })
  interbankKey: string;

  @Column('character varying', {
    length: 50,
    name: 'status',
    nullable: false,
  })
  status: string;

  @Column('boolean', {
    name: 'is_active',
    nullable: false,
  })
  isActive: boolean;

  @Column('numeric', {
    name: 'money',
    nullable: false,
  })
  money: number;

  @Column('integer', {
    name: 'client_id',
    nullable: true,
  })
  clientId?: number;

  @Column('integer', {
    name: 'account_type_id',
    nullable: true,
  })
  accountTypeId?: number;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToOne(() => Client, (client) => client.accounts)
  client?: Client;

  @ManyToOne(() => AccountType, (accountType) => accountType.accounts)
  accountType?: AccountType;

  @OneToMany(() => Card, (card) => card.account)
  cards?: Card[];

  @OneToMany(() => Transaction, (transaction) => transaction.account)
  transactions?: Transaction[];
}
