import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { Transaction } from './Transaction';

@Entity()
export class TransactionType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    length: 50,
    name: 'code',
    nullable: false,
    unique: true,
  })
  code: string;

  @Column('character varying', {
    length: 50,
    name: 'name',
    nullable: false,
  })
  name: string;

  @Column('character varying', {
    length: 150,
    name: 'description',
    nullable: false,
  })
  description: string;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @OneToMany(() => Transaction, (transaction) => transaction.transactionType)
  transactions?: Transaction[];
}
