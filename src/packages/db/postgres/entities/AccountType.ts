import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { Account } from './Account';

@Entity()
export class AccountType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    length: 50,
    name: 'code',
    nullable: false,
    unique: true,
  })
  code: string;

  @Column('character varying', {
    length: 50,
    name: 'name',
    nullable: false,
  })
  name: string;

  @Column('character varying', {
    length: 150,
    name: 'description',
    nullable: false,
  })
  description: string;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @OneToMany(() => Account, (account) => account.accountType)
  accounts?: Account[];
}
