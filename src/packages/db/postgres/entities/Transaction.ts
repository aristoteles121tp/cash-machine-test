import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';
import { StatusTransaction } from './StatusTransaction';
import { TransactionType } from './TransactionType';
import { Account } from './Account';

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('timestamp without time zone', {
    name: 'date',
    nullable: false,
  })
  date: Date;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToOne(
    () => StatusTransaction,
    (statusTransaction) => statusTransaction.transactions,
  )
  statusTransaction?: StatusTransaction;

  @ManyToOne(
    () => TransactionType,
    (transactionType) => transactionType.transactions,
  )
  transactionType?: TransactionType;

  @ManyToOne(() => Account, (account) => account.transactions)
  account?: Account;
}
