import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Role {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    name: 'type',
    nullable: false,
    length: 50,
    unique: true,
  })
  type: string;

  @Column('character varying', {
    name: 'name',
    length: 50,
    nullable: false,
  })
  name: string;

  @Column('character varying', {
    name: 'description',
    length: 100,
    nullable: true,
  })
  description: string;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;
}
