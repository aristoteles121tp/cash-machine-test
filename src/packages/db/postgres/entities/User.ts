import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  BeforeInsert,
  ManyToMany,
  JoinTable,
  OneToOne,
  JoinColumn,
  Index,
} from 'typeorm';
import { HashTools } from '../../../core/utils';
import { Role } from './Role';
import { Client } from './Client';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    length: 100,
    name: 'email',
    nullable: false,
    unique: true,
  })
  @Index({ unique: true })
  email: string;

  @Column('character varying', {
    length: 75,
    name: 'password',
    nullable: false,
  })
  password: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await new HashTools().hashText(this.password);
  }

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToMany(() => Role)
  @JoinTable()
  roles?: Role[];

  @OneToOne(() => Client)
  @JoinColumn()
  client?: Client;
}
