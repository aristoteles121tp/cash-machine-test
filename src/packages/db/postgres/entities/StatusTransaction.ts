import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { Transaction } from './Transaction';

@Entity()
export class StatusTransaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    length: 50,
    name: 'type',
    nullable: false,
    unique: true,
  })
  type: string;

  @Column('character varying', {
    length: 50,
    name: 'name',
    nullable: false,
  })
  name: string;

  @Column('character varying', {
    length: 150,
    name: 'description',
    nullable: false,
  })
  description: string;

  @CreateDateColumn()
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @OneToMany(() => Transaction, (transaction) => transaction.statusTransaction)
  transactions?: Transaction[];
}
