import { MigrationInterface, QueryRunner } from 'typeorm';
import { statusTransactionType } from '../../../core/constants';

export class statusTransactionSeeds1657564010763 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.insert('status_transaction', [
      {
        type: statusTransactionType.pending,
        name: 'Pending',
        description: 'Transaction in status pending',
      },
      {
        type: statusTransactionType.inProcess,
        name: 'In process',
        description: 'Transaction in status in process',
      },
      {
        type: statusTransactionType.completed,
        name: 'Completed',
        description: 'Transaction in status completed',
      },
      {
        type: statusTransactionType.canceled,
        name: 'canceled',
        description: 'Transaction in status canceled',
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete('status_transaction', null);
  }
}
