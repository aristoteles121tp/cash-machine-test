import { MigrationInterface, QueryRunner } from 'typeorm';
import { transactionTypeCode } from '../../../core/constants';

export class transactionTypeSeeds1657564028027 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.insert('transaction_type', [
      {
        code: transactionTypeCode.depositMoney,
        name: 'Deposit',
        description: 'Deposit money',
      },
      {
        code: transactionTypeCode.takeOutMoney,
        name: 'To take out',
        description: 'To take money out',
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete('transaction_type', null);
  }
}
