import { MigrationInterface, QueryRunner } from 'typeorm';
import { roleType } from '../../../core';

export class roleSeeds1657517146171 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.insert('role', [
      {
        type: roleType.client,
        name: 'Client',
        description: 'Clients banks',
      },
      {
        type: roleType.employee,
        name: 'Employee',
        description: 'Clients banks',
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete('role', null);
  }
}
