import { MigrationInterface, QueryRunner } from 'typeorm';
import { cardTypeCode } from '../../../core/constants';

export class cardTypeSeeds1657563986902 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.insert('card_type', [
      {
        code: cardTypeCode.debitBasic,
        name: 'Basic debit card',
        description: 'Basic debit card',
      },
      {
        code: cardTypeCode.creditBlue,
        name: 'Blue credit card',
        description: 'Blue credit card',
      },
      {
        code: cardTypeCode.creditGold,
        name: 'Gold credit card',
        description: 'Gold credit card',
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete('card_type', null);
  }
}
