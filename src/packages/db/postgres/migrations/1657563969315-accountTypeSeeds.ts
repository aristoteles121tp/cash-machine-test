import { MigrationInterface, QueryRunner } from 'typeorm';
import { accountTypeCode } from '../../../core/constants';

export class accountTypeSeeds1657563969315 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.insert('account_type', [
      {
        code: accountTypeCode.checks,
        name: 'Checks account',
        description: 'Checks account',
      },
      {
        code: accountTypeCode.paysheet,
        name: 'Paysheet account',
        description: 'Paysheet account',
      },
      {
        code: accountTypeCode.credit,
        name: 'Credit account',
        description: 'Credit account',
      },
      {
        code: accountTypeCode.debit,
        name: 'Debit account',
        description: 'Debit account',
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.delete('account_type', null);
  }
}
