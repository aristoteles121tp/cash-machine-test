import {
  AccountCrud,
  AccountTypeCrud,
  TransactionTypeCrud,
  StatusTransactionCrud,
  TransactionCrud,
  ClientCrud,
} from '../../../core/crud';
import { AccountController } from './account.controller';
import { RepositoryModule } from '../../../db';
import { AccountUsecase } from '../../../core/usecase';
import { TransactionServices, AccountService } from '../../../core/services';
import { AuthenticationMiddleware } from '../../middlewares';
import { JWTUtil } from '../../../core/utils';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

@Module({
  imports: [RepositoryModule],
  providers: [
    AccountUsecase,
    AccountCrud,
    AccountTypeCrud,
    TransactionTypeCrud,
    StatusTransactionCrud,
    TransactionCrud,
    AccountService,
    TransactionServices,
    JWTUtil,
    ClientCrud,
  ],
  controllers: [AccountController],
})
export class AccountServiceModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthenticationMiddleware).forRoutes('account');
  }
}
