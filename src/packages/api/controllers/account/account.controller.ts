import { Body, Controller, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { AccountUsecase } from '../../../core/usecase';
import { Response, Request } from 'express';
import {
  DTOTakeOutMoneyOfAccountValidation,
  DTODepositMoneyOfAccountValidation,
} from '../../validators';

@Controller('/account')
export class AccountController {
  constructor(private readonly accountUseCase: AccountUsecase) {}

  @Post('/take-out-money')
  async takeMoney(
    @Body() data: DTOTakeOutMoneyOfAccountValidation,
    @Req() request: Request,
    @Res() response: Response,
  ): Promise<any> {
    try {
      const transactionIsComplete: boolean =
        await this.accountUseCase.takeMoneyOfAccount(
          (request as any).client.id,
          data,
        );
      if (transactionIsComplete) {
        response.status(HttpStatus.OK).send({
          message: 'Money taken out of account successful.',
        });
      } else {
        response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .send(
            'You cannot take out more money than you have in your account.',
          );
      }
    } catch (error) {
      response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Something went wrong, please try again later.');
    }
  }

  @Post('/deposit-money')
  async depositMoney(
    @Body() data: DTODepositMoneyOfAccountValidation,
    @Req() request: Request,
    @Res() response: Response,
  ): Promise<any> {
    try {
      await this.accountUseCase.depositMoneyOfAccount(
        (request as any).client.id,
        data,
      );
      response.status(HttpStatus.OK).send('Deposit to account successful.');
    } catch (error) {
      response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Something went wrong, please try again later.');
    }
  }
}
