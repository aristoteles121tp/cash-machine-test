import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { SignupUseCase } from '../../../core/usecase';
import {
  IResponseSignup,
  ISignupClientResponse,
} from '../../../core/interfaces';
import { Response } from 'express';
import { DTOSignupValidation } from '../../validators';
import { JWTUtil } from '../../../core/utils';

@Controller('/client')
export class ClientController {
  constructor(
    private readonly signupUseCase: SignupUseCase,
    private readonly jwtUtils: JWTUtil,
  ) {}

  @Post('/signup')
  async signup(
    @Body() data: DTOSignupValidation,
    @Res() response: Response,
  ): Promise<IResponseSignup> {
    try {
      const result: ISignupClientResponse =
        await this.signupUseCase.signUpClient(data);
      console.debug(JSON.stringify(result, null, 4));
      const accessToken = this.jwtUtils.getTokenBearer<any>({
        client: {
          id: result.client.id,
        },
      });
      const res: IResponseSignup = {
        client: {
          accessToken,
          accounts: {
            debit: {
              number: result.debitCard.account.number,
              type: result.debitCard.account.accountType.code,
              card: {
                number: result.debitCard.number,
              },
            },
            credit: {
              number: result.creditCard.account.number,
              type: result.creditCard.account.accountType.code,
              card: {
                number: result.creditCard.number,
              },
            },
          },
        },
      };
      response.status(HttpStatus.CREATED).json(res);
      return res;
    } catch (error) {
      console.error(JSON.stringify(error, null, 4));
      response
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send('Something went wrong, please try again later');
    }
  }
}
