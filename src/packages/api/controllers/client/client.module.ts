import {
  SignupUseCase,
  UserCrud,
  ClientCrud,
  RoleCrud,
  AccountCrud,
  AccountTypeCrud,
  CardCrud,
  CardTypeCrud,
  CardServices,
  ClientServices,
  AccountService,
} from '../../../core';
import { ClientController } from './client.controller';
import { RepositoryModule } from '../../../db';
import { JWTUtil } from '../../../core/utils';
import { Module } from '@nestjs/common';

@Module({
  imports: [RepositoryModule],
  providers: [
    SignupUseCase,
    ClientCrud,
    UserCrud,
    RoleCrud,
    AccountCrud,
    AccountTypeCrud,
    CardCrud,
    CardTypeCrud,
    CardServices,
    ClientServices,
    AccountService,
    JWTUtil,
  ],
  controllers: [ClientController],
})
export class ClientSrviceModule {}
