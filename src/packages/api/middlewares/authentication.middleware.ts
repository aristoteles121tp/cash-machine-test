import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JWTUtil } from '../../core/utils';
import { ClientCrud } from '../../core/crud';
import { Client } from '../../db/postgres/entities';

@Injectable()
export class AuthenticationMiddleware implements NestMiddleware {
  constructor(
    private readonly clientCrud: ClientCrud,
    private readonly jwtUtils: JWTUtil,
  ) {}
  async use(req: Request, res: Response, next: NextFunction) {
    const logMsg = '[validateJWT]';
    try {
      const authorization: string = req.headers.authorization as string;
      const accessToken: string = authorization.split(' ')[1];
      console.info(
        `${logMsg} accessToken:`,
        JSON.stringify({ accessToken, authorization }, null, 4),
      );
      const payload = this.jwtUtils.verifyJWTAndGetData(accessToken);
      console.info(`${logMsg} Payload:`, JSON.stringify(payload, null, 4));
      if (!payload.client.id) {
        return next(new Error('Acces token is not valid.'));
      }
      const [clientFound]: Client[] = await this.clientCrud.readMany({
        id: payload.client.id,
      });
      if (!clientFound) {
        return next(new Error('Acces token is not valid.'));
      }
      (req as any).client = clientFound;
      return next();
    } catch (error) {
      console.error(`${logMsg} Error to validate JWT`, error);
      return res.status(HttpStatus.UNAUTHORIZED).json({
        error: {
          message: 'Error to validate access token.',
          details: (error as any).message,
        },
      });
    }
  }
}
