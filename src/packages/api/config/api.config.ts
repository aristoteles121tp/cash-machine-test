import { Request, Response, NextFunction, urlencoded, json } from 'express';
import * as morgan from 'morgan';
import * as moment from 'moment';
import * as shortid from 'shortid';

function enableCoors(app: any) {
  app.enableCors({
    origin: '*',
    methods: 'GET, PUT, POST, DELETE, PATCH, OPTIONS',
    allowedHeaders:
      'Origin, X-Requested-With, Content-Type, Accept, Accept-Language',
  });
}
function assignIdToRequest(req: Request, res: Response, next: NextFunction) {
  (req as any).id = shortid.generate();
  return next();
}

const logIncomingRequest = (logger: any) =>
  morgan(
    (tokens: morgan.TokenIndexer, req: Request, res: Response) => {
      return [
        '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
        '\n' + moment().format('YYYY-MM-DD HH:mm:ss'),
        `[ ${(req as any).id} ]`,
        tokens.method(req, res),
        tokens.url(req, res),
        `\nbody: ${JSON.stringify(req.body || {}, null, 4)}`,
        `\nquery: ${JSON.stringify(req.query || {}, null, 4)}`,
      ].join(' ');
    },
    { immediate: true, stream: logger.stream as any },
  );

const logRequestResponse = (logger: any) =>
  morgan(
    (tokens: morgan.TokenIndexer, req: Request, res: Response) => {
      return [
        moment().format('YYYY-MM-DD HH:mm:ss'),
        `[ ${(req as any).id} ]`,
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'),
        '-',
        tokens['response-time'](req, res),
        'ms',
        '\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~',
      ].join(' ');
    },
    { stream: logger.stream as any },
  );

export const setApiSetUp = (app: any, logger: any) => {
  enableCoors(app);
  app.use(urlencoded({ extended: false }));
  app.use(json());
  app.use(assignIdToRequest);
  app.use(logIncomingRequest(logger));
  app.use(logRequestResponse(logger));
};
