import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
import { accountTypeCode } from '../../core/constants';
import { IDTOTakeOutMoneyOfAccountInput } from '../../core/interfaces';

export class DTOTakeOutMoneyOfAccountValidation
  implements IDTOTakeOutMoneyOfAccountInput
{
  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsNumber(
    {},
    {
      message: 'El campo $property debe ser del tipo numerico',
    },
  )
  amountToTake: number;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  accountNumber: string;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  accountType: accountTypeCode;
}
