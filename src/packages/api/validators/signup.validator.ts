import { IsEmail, IsNotEmpty, IsString, IsOptional } from 'class-validator';
import { roleType } from '../../core/constants';
import { IDTOSignup } from '../../core/interfaces';

export class DTOSignupValidation implements IDTOSignup {
  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  name: string;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  firstName: string;

  @IsOptional()
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  lastName: string;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsEmail(
    {},
    {
      message:
        'El campo $property no contien el formato de un correo electrónico',
    },
  )
  email: string;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  password: string;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  role: roleType;

  @IsOptional()
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  address?: string;
}
