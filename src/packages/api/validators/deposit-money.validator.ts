import { IsNotEmpty, IsString, IsNumber } from 'class-validator';
import { IDTODepositMoneyOfAccountInput } from '../../core/interfaces';
import { accountTypeCode } from '../../core/constants';

export class DTODepositMoneyOfAccountValidation
  implements IDTODepositMoneyOfAccountInput
{
  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsNumber(
    {},
    {
      message: 'El campo $property debe ser del tipo numerico',
    },
  )
  amountToDeposit: number;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  accountNumber: string;

  @IsNotEmpty({ message: 'El campo $property es requerido' })
  @IsString({
    message: 'El campo $property debe ser del tipo cadena',
  })
  accountType: accountTypeCode;
}
