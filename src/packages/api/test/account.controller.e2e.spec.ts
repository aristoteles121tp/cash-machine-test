import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../../app.module';
import { IResponseSignup } from '../../core/interfaces';
import { faker } from '@faker-js/faker';

describe('Account controller (e2e)', () => {
  let app: INestApplication;

  const signUpClientFake = async (): Promise<IResponseSignup> =>
    (
      await request(app.getHttpServer()).post('/client/signup').send({
        name: faker.name.findName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: '123456',
        role: 'client',
        address: faker.address.streetAddress(),
      })
    ).body;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/account/take-out-money (POST) (e2e)', () => {
    it('should take $100 of debit account', async () => {
      const {
        client: {
          accessToken,
          accounts: { debit: debbitAccount },
        },
      }: IResponseSignup = await signUpClientFake();
      await request(app.getHttpServer())
        .post('/account/take-out-money')
        .set({
          authorization: `Bearer ${accessToken}`,
        })
        .send({
          accountNumber: debbitAccount.number,
          amountToTake: 100,
          accountType: debbitAccount.type,
        })
        .expect(HttpStatus.OK);
    });

    it('should take $100 of credit account', async () => {
      const {
        client: {
          accessToken,
          accounts: { credit: creditAccount },
        },
      }: IResponseSignup = await signUpClientFake();
      await request(app.getHttpServer())
        .post('/account/take-out-money')
        .set({
          authorization: `Bearer ${accessToken}`,
        })
        .send({
          accountNumber: creditAccount.number,
          amountToTake: 100,
          accountType: creditAccount.type,
        })
        .expect(HttpStatus.OK);
    });
  });

  describe('/account/deposit-money (POST) (e2e)', () => {
    it('should deposit $200 in debit account', async () => {
      const {
        client: {
          accessToken,
          accounts: { debit: debbitAccount },
        },
      }: IResponseSignup = await signUpClientFake();
      await request(app.getHttpServer())
        .post('/account/deposit-money')
        .set({
          authorization: `Bearer ${accessToken}`,
        })
        .send({
          accountNumber: debbitAccount.number,
          amountToDeposit: 200,
          accountType: debbitAccount.type,
        })
        .expect(HttpStatus.OK);
    });

    it('should deposit $200 in credit account', async () => {
      const {
        client: {
          accessToken,
          accounts: { credit: creditAccount },
        },
      }: IResponseSignup = await signUpClientFake();
      await request(app.getHttpServer())
        .post('/account/deposit-money')
        .set({
          authorization: `Bearer ${accessToken}`,
        })
        .send({
          accountNumber: creditAccount.number,
          amountToDeposit: 200,
          accountType: creditAccount.type,
        })
        .expect(HttpStatus.OK);
    });
  });
});
