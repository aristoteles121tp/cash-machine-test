import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../../app.module';
import { accountTypeCode } from '../../core/constants';
import { faker } from '@faker-js/faker';

describe('Signup client (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/client/signup (POST)', async () => {
    const result = await request(app.getHttpServer())
      .post('/client/signup')
      .send({
        name: faker.name.findName(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        password: '123456',
        role: 'client',
        address: faker.address.streetAddress(),
      });
    expect(result.status).toEqual(HttpStatus.CREATED);
    expect(result.body.client).toBeTruthy();
    expect(result.body.client.accessToken).toBeTruthy();
    expect(result.body.client.accounts).toBeTruthy();
    expect(result.body.client.accounts.debit).toBeTruthy();
    expect(result.body.client.accounts.debit.number).toBeTruthy();
    expect(result.body.client.accounts.debit.type).toEqual(
      accountTypeCode.debit,
    );
    expect(result.body.client.accounts.debit.card).toBeTruthy();
    expect(result.body.client.accounts.debit.card.number).toBeTruthy();
    expect(result.body.client.accounts.credit).toBeTruthy();
    expect(result.body.client.accounts.credit.number).toBeTruthy();
    expect(result.body.client.accounts.credit.type).toEqual(
      accountTypeCode.credit,
    );
    expect(result.body.client.accounts.credit.card).toBeTruthy();
    expect(result.body.client.accounts.credit.card.number).toBeTruthy();
  });
});
