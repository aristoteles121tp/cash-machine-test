import * as bcrypt from 'bcrypt';
import { IBoundaryHashTools } from '../interfaces';

export class HashTools implements IBoundaryHashTools {
  private saltRounds: number;
  constructor() {
    this.saltRounds = parseInt(process.env.ENCRYPT_SALT_ROUNDS, 10) || 10;
  }

  async hashText(text: string): Promise<string> {
    return await bcrypt.hash(text, this.saltRounds);
  }

  async compareText(text: string, textHash: string): Promise<boolean> {
    return await bcrypt.compare(text, textHash);
  }
}
