import * as JWT from 'jsonwebtoken';
import * as randToken from 'rand-token';
import * as fs from 'fs';
import * as path from 'path';
import { Injectable } from '@nestjs/common';
// import { IJWTData } from './../interfaces';

interface IJWTData {
  refreshToken: string;
  seessionTimeInMinutes: number;
  tokenRefreshExpiryInHours: number;
  algorithm: JWT.Algorithm;
}

@Injectable()
export class JWTUtil {
  private algorithm: JWT.Algorithm;
  private sessionTimeInMinutes: number;
  private tokenRefreshExpiryInHours: number;
  private privateKey: Buffer;
  private publicKey: Buffer;

  constructor() {
    this.algorithm = (process.env.TOKEN_ALGOTITHM as JWT.Algorithm) || 'RS256';
    this.sessionTimeInMinutes = parseInt(
      process.env.TOKEN_EXPIRY_MINUTES || '30',
      10,
    );
    this.tokenRefreshExpiryInHours = parseInt(
      process.env.TOKEN_REFRESH_EXPIRY_HOURS || '24',
      10,
    );
    (this.privateKey = fs.readFileSync(
      path.resolve(
        __dirname,
        './../../api/config/keys/jwt/web_services_private.key',
      ),
    )),
      (this.publicKey = fs.readFileSync(
        path.resolve(
          __dirname,
          './../../api/config/keys/jwt/web_services_public.key',
        ),
      ));
  }

  getDataForGenerateTokenBearer(): IJWTData {
    return {
      algorithm: this.algorithm,
      seessionTimeInMinutes: this.sessionTimeInMinutes,
      refreshToken: randToken.generate(60),
      tokenRefreshExpiryInHours: this.tokenRefreshExpiryInHours,
    };
  }

  getTokenBearer<T>(payload: T): string {
    return JWT.sign(payload as any, this.privateKey, {
      expiresIn: `${this.sessionTimeInMinutes}m`,
      algorithm: this.algorithm,
    } as JWT.SignOptions);
  }

  verifyJWTAndGetData(jwt: string): any {
    return JWT.verify(jwt, this.publicKey, { algorithms: [this.algorithm] });
  }

  getDataFromJWT(jwt: string): any {
    return JWT.decode(jwt, { complete: true });
  }
}
