export const getRandomNumber = (length: number): string => {
  return `${Math.floor(Math.random() * Math.pow(10, length))}`;
};
