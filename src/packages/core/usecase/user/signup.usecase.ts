import {
  ClientCrud,
  UserCrud,
  RoleCrud,
  AccountCrud,
  AccountTypeCrud,
  CardCrud,
  CardTypeCrud,
} from '../../crud';
import {
  accountTypeCode,
  defaultMoneyInDebitAccount,
  defaultMoneyInCreditAccount,
  cardTypeCode,
} from '../../constants';
import { IDTOSignup, ISignupClientResponse } from '../../interfaces';
import { Injectable, Logger } from '@nestjs/common';
import { CardServices, ClientServices, AccountService } from '../../services';

const logger = new Logger('SignupUseCase');

@Injectable()
export class SignupUseCase {
  constructor(
    private readonly userCrud: UserCrud,
    private readonly clientCrud: ClientCrud,
    private readonly roleCrud: RoleCrud,
    private readonly accountCrud: AccountCrud,
    private readonly accountTypeCrud: AccountTypeCrud,
    private readonly cardCrud: CardCrud,
    private readonly cardTypeCrud: CardTypeCrud,
    private readonly cardServices: CardServices,
    private readonly clientServices: ClientServices,
    private readonly accountServices: AccountService,
  ) {}

  async signUpClient(data: IDTOSignup): Promise<ISignupClientResponse> {
    try {
      const client = await this.clientServices.createClient(
        data,
        this.userCrud,
        this.clientCrud,
        this.roleCrud,
      );
      logger.log('Client created', client);

      const [debitAccount, creditAccount] =
        await this.accountServices.createAccountsToClient(
          [
            {
              clientId: client.id,
              accountTypeCode: accountTypeCode.debit,
              money: defaultMoneyInDebitAccount,
            },
            {
              clientId: client.id,
              accountTypeCode: accountTypeCode.credit,
              money: defaultMoneyInCreditAccount,
            },
          ],
          this.accountTypeCrud,
          this.accountCrud,
        );
      logger.log('Debit account created', { ...debitAccount });
      logger.log('Credit account created', { ...creditAccount });

      const [debitCard, creditCard] =
        await this.cardServices.createCardsToAccounts(
          [
            {
              account: debitAccount,
              cardTypeCode: cardTypeCode.debitBasic,
            },
            {
              account: creditAccount,
              cardTypeCode: cardTypeCode.creditBlue,
            },
          ],
          this.cardCrud,
          this.cardTypeCrud,
        );

      return {
        client,
        debitCard: debitCard,
        creditCard: creditCard,
      };
    } catch (error) {
      logger.error('Error ti signup client', error);
      throw error;
    }
  }
}
