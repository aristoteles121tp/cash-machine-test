import { Injectable } from '@nestjs/common';
import {
  IDTOTakeOutMoneyOfAccountInput,
  IDTODepositMoneyOfAccountInput,
} from '../../interfaces';
import {
  AccountCrud,
  AccountTypeCrud,
  TransactionTypeCrud,
  StatusTransactionCrud,
  TransactionCrud,
} from '../../crud';
import { Account, Transaction } from '../../../db/postgres/entities';
import { Logger } from '@nestjs/common';
import {
  statusTransactionType,
  transactionTypeCode,
  coutePercentageToTakeMoneyInCreditAccount,
  accountTypeCode,
} from '../../constants';
import { AccountService, TransactionServices } from '../../services';

const logger = new Logger('AccountService');

@Injectable()
export class AccountUsecase {
  constructor(
    private readonly accountCrud: AccountCrud,
    private readonly accountTypeCrud: AccountTypeCrud,
    private readonly transactionTypeCrud: TransactionTypeCrud,
    private readonly statusTransactionCrud: StatusTransactionCrud,
    private readonly transactionCrud: TransactionCrud,
    private readonly accountServices: AccountService,
    private readonly transactionServices: TransactionServices,
  ) {}

  async takeMoneyOfAccount(
    clientId: number,
    data: IDTOTakeOutMoneyOfAccountInput,
  ): Promise<boolean> {
    const accountFound: Account =
      await this.accountServices.getAccountOfClientByTypeAndNumber(
        clientId,
        data.accountType,
        data.accountNumber,
        this.accountTypeCrud,
        this.accountCrud,
      );

    let moneyResult: number = +accountFound.money - +data.amountToTake;
    logger.log(`moneyResult -> ${moneyResult}`);

    if (data.accountType === accountTypeCode.credit) {
      const cuoteToTakeMoney: number =
        +data.amountToTake * (coutePercentageToTakeMoneyInCreditAccount / 100);
      moneyResult =
        +accountFound.money - (+data.amountToTake + cuoteToTakeMoney);
    }

    if (moneyResult > 0) {
      const accountUpdated: Account = await this.accountCrud.updateOne(
        { id: accountFound.id },
        { money: moneyResult },
      );
      logger.log('accountUpdated', { ...accountUpdated });

      const transaction: Transaction =
        await this.transactionServices.createTransaction(
          accountFound,
          statusTransactionType.completed,
          transactionTypeCode.takeOutMoney,
          this.transactionTypeCrud,
          this.statusTransactionCrud,
          this.transactionCrud,
        );
      logger.log('transaction', transaction);
      return true;
    } else {
      return false;
    }
  }

  async depositMoneyOfAccount(
    clientId: number,
    data: IDTODepositMoneyOfAccountInput,
  ): Promise<boolean> {
    const accountFound: Account =
      await this.accountServices.getAccountOfClientByTypeAndNumber(
        clientId,
        data.accountType,
        data.accountNumber,
        this.accountTypeCrud,
        this.accountCrud,
      );

    const totalMoney: number = +accountFound.money + +data.amountToDeposit;
    const accountUpdated: Account = await this.accountCrud.updateOne(
      { id: accountFound.id },
      { money: totalMoney },
    );
    logger.log('accountUpdated', { ...accountUpdated });

    const transaction: Transaction =
      await this.transactionServices.createTransaction(
        accountFound,
        statusTransactionType.completed,
        transactionTypeCode.depositMoney,
        this.transactionTypeCrud,
        this.statusTransactionCrud,
        this.transactionCrud,
      );
    logger.log('transaction', transaction);
    return true;
  }
}
