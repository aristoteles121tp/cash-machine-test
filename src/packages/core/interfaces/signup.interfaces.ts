import { roleType, cardTypeCode, accountTypeCode } from '../constants/enums';
import {
  CardType,
  Account,
  Card,
  AccountType,
  Client,
} from '../../db/postgres/entities';

export interface IDTOSignup {
  email: string;
  password: string;
  firstName: string;
  lastName?: string;
  name: string;
  address?: string;
  role: roleType;
}

export interface IResponseSignup {
  client: {
    accessToken: string;
    accounts: {
      debit: {
        number: string;
        type: string;
        card: {
          number: string;
        };
      };
      credit: {
        number: string;
        type: string;
        card: {
          number: string;
        };
      };
    };
  };
}

export type ICardTypeByCode = {
  [keyof in cardTypeCode]: CardType;
};

export type IAccountTypeByCode = {
  [keyof in accountTypeCode]: AccountType;
};

export interface ICreateCardToAccountInput {
  account: Account;
  cardTypeCode: cardTypeCode;
}

export interface ICreateAccountToClientInput {
  clientId: number;
  accountTypeCode: accountTypeCode;
  money: number;
}

export interface ISignupClientResponse {
  debitCard: Card;
  creditCard: Card;
  client: Client;
}
