import { accountTypeCode } from '../constants';

interface IBasicDataToTransactionsInAccount {
  accountNumber: string;
  accountType: accountTypeCode;
}

export interface IDTOTakeOutMoneyOfAccountInput
  extends IBasicDataToTransactionsInAccount {
  amountToTake: number;
}

export interface IDTODepositMoneyOfAccountInput
  extends IBasicDataToTransactionsInAccount {
  amountToDeposit: number;
}
