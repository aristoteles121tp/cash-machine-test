export interface IBoundaryHashTools {
  hashText(text: string): Promise<string>;
  compareText(text: string, textHash: string): Promise<boolean>;
}
