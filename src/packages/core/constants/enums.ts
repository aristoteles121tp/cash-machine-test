enum roleType {
  client = 'client',
  employee = 'employee',
}

enum accountTypeCode {
  paysheet = 'paysheet',
  checks = 'checks',
  credit = 'credit',
  debit = 'debit',
}

enum accountStatus {
  active = 'active',
  canceled = 'canceled',
  inValidation = 'inValidation',
}

enum cardTypeCode {
  debitBasic = 'debitBasic',
  creditBlue = 'creditBlue',
  creditGold = 'creditGold',
}

enum cardStatus {
  inCreation = 'inCreation',
  sent = 'sent',
  received = 'recived',
  activated = 'activated',
  canceled = 'canceled',
}

enum statusTransactionType {
  pending = 'pending',
  inProcess = 'inProcess',
  completed = 'completed',
  canceled = 'canceled',
}

enum transactionTypeCode {
  depositMoney = 'depositMoney',
  takeOutMoney = 'takeOutMoney',
}

export {
  roleType,
  accountTypeCode,
  cardTypeCode,
  statusTransactionType,
  transactionTypeCode,
  accountStatus,
  cardStatus,
};
