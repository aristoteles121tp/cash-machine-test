import { ICardTypeByCode, ICreateCardToAccountInput } from '../interfaces';
import { CardType, Card } from '../../db/postgres/entities';
import { CardTypeCrud, CardCrud } from '../crud';
import { cardStatus } from '../constants';
import * as moment from 'moment';
import { getRandomNumber } from '../utils';
import { Logger } from '@nestjs/common';

const logger = new Logger('CardServices');

export class CardServices {
  async getCardTypesByCodes(
    cardTypeCrud: CardTypeCrud,
  ): Promise<ICardTypeByCode> {
    const cardTypes: CardType[] = await cardTypeCrud.readMany();
    const cardTypesByCodes: ICardTypeByCode = {
      creditBlue: null,
      creditGold: null,
      debitBasic: null,
    };
    cardTypes.forEach(
      (cardType) => (cardTypesByCodes[cardType.code] = cardType),
    );
    logger.log('cardTypesByCode', { ...cardTypesByCodes });
    return cardTypesByCodes;
  }

  async createCardsToAccounts(
    data: ICreateCardToAccountInput[],
    cardCrud: CardCrud,
    cardTypeCrud: CardTypeCrud,
  ): Promise<Card[]> {
    const cardTypesByCode: ICardTypeByCode = await this.getCardTypesByCodes(
      cardTypeCrud,
    );
    logger.log('cardTypesByCode', { ...cardTypesByCode });

    const defaultParamsToCreateCard: Partial<Card> = {
      status: cardStatus.activated,
      isActive: true,
      expiryDate: moment().add(4, 'years').toDate(),
    };

    const cardsCreateds: Card[] = await Promise.all(
      data.map(({ cardTypeCode, account }) =>
        cardCrud.createOne({
          ...defaultParamsToCreateCard,
          cardType: cardTypesByCode[cardTypeCode],
          account,
          cvc: getRandomNumber(3),
          nip: getRandomNumber(4),
          number: getRandomNumber(16),
        }),
      ),
    );
    logger.log('cardsCreateds', { ...cardsCreateds });
    return cardsCreateds;
  }
}
