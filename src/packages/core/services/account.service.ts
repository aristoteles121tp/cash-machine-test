import { Account, AccountType } from '../../db/postgres/entities';
import { AccountTypeCrud, AccountCrud } from '../crud';
import { getRandomNumber } from '../utils';
import { accountStatus, accountTypeCode } from '../constants';
import { Logger } from '@nestjs/common';
import { IAccountTypeByCode, ICreateAccountToClientInput } from '../interfaces';

const logger = new Logger('AccountService');

export class AccountService {
  async getAccountTypesByCodes(
    accountTypeCrud: AccountTypeCrud,
  ): Promise<IAccountTypeByCode> {
    const accountTypes: AccountType[] = await accountTypeCrud.readMany();
    const accountTypesByCodes: IAccountTypeByCode = {
      checks: null,
      credit: null,
      debit: null,
      paysheet: null,
    };
    accountTypes.forEach(
      (accountType) => (accountTypesByCodes[accountType.code] = accountType),
    );
    logger.log('accountTypesByCodes', { ...accountTypesByCodes });
    return accountTypesByCodes;
  }

  async createAccountsToClient(
    data: ICreateAccountToClientInput[],
    accountTypeCrud: AccountTypeCrud,
    accountCrud: AccountCrud,
  ): Promise<Account[]> {
    const accountTypesByCode: IAccountTypeByCode =
      await this.getAccountTypesByCodes(accountTypeCrud);
    logger.log('accountTypesByCode', { ...accountTypesByCode });

    const defaultParamsToCreateAccount: Partial<Account> = {
      status: accountStatus.active,
      isActive: true,
    };

    const accountsCreateds: Account[] = await Promise.all(
      data.map(({ accountTypeCode, money, clientId }) => {
        const numberAccount = getRandomNumber(9);
        const interbankKey = `${getRandomNumber(
          2,
        )}${numberAccount}${getRandomNumber(1)}`;
        const accountType: AccountType = accountTypesByCode[accountTypeCode];
        return accountCrud.createOne({
          ...defaultParamsToCreateAccount,
          clientId,
          accountType,
          accountTypeId: accountType.id,
          money,
          interbankKey,
          number: numberAccount,
        });
      }),
    );
    logger.log('accountsCreateds', { ...accountsCreateds });
    return accountsCreateds;
  }

  async getAccountOfClientByTypeAndNumber(
    clientId: number,
    accountTypeCode: accountTypeCode,
    accountNumber: string,
    accountTypeCrud: AccountTypeCrud,
    accountCrud: AccountCrud,
  ): Promise<Account> {
    const [accountType] = await accountTypeCrud.readMany({
      code: accountTypeCode,
    });
    logger.log('accountType', { ...accountType });

    const [accountFound] = await accountCrud.readMany({
      number: accountNumber,
      clientId,
      accountTypeId: accountType.id,
    });
    logger.log('accountFound', { ...accountFound });

    if (!accountFound) {
      logger.error('Account not found.', { ...accountFound });
      throw new Error('Account not found.');
    }
    return accountFound;
  }
}
