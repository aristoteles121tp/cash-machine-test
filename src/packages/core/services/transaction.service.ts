import { Transaction, Account } from '../../db/postgres/entities';
import {
  TransactionTypeCrud,
  StatusTransactionCrud,
  TransactionCrud,
} from '../crud';
import * as moment from 'moment';
import { transactionTypeCode, statusTransactionType } from '../constants';
import { Logger } from '@nestjs/common';

const logger = new Logger('TransactionServices');

export class TransactionServices {
  async createTransaction(
    account: Account,
    statusTransactionTypeFound: statusTransactionType,
    transactionTypeCodeFound: transactionTypeCode,
    transactionTypeCrud: TransactionTypeCrud,
    statusTransactionCrud: StatusTransactionCrud,
    transactionCrud: TransactionCrud,
  ): Promise<Transaction> {
    const [transactionType] = await transactionTypeCrud.readMany({
      code: transactionTypeCodeFound,
    });
    logger.log('transactionType', { ...transactionType });

    const [statusTransaction] = await statusTransactionCrud.readMany({
      type: statusTransactionTypeFound,
    });
    logger.log('statusTransaction', { ...statusTransaction });

    const transaction: Transaction = await transactionCrud.createOne({
      date: moment().toDate(),
      account,
      statusTransaction,
      transactionType,
    });
    logger.log('transaction', transaction);
    return transaction;
  }
}
