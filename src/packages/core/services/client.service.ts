import { IDTOSignup } from '../interfaces';
import { Client, User } from '../../db/postgres/entities';
import { roleType } from '../constants';
import { ClientCrud, RoleCrud, UserCrud } from '../crud';
import { Logger } from '@nestjs/common';

const logger = new Logger('ClientServices');

export class ClientServices {
  async createClient(
    data: IDTOSignup,
    userCrud: UserCrud,
    clientCrud: ClientCrud,
    roleCrud: RoleCrud,
  ): Promise<Client> {
    const [roleClient] = await roleCrud.readMany({
      type: roleType.client,
    });
    logger.log('Role client found', { ...roleClient });
    const [client] = await clientCrud.createMany([
      {
        firstName: data.firstName,
        lastName: data.lastName,
        name: data.name,
        address: data.address,
      },
    ]);
    logger.log('Client created', client);
    const user: User = await userCrud.createOne({
      email: data.email,
      client,
      roles: [roleClient],
      password: data.password,
    });
    logger.log('User created', { ...user });
    return client;
  }
}
