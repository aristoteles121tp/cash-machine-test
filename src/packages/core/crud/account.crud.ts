import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { Account } from '../../db/postgres/entities';

@Injectable()
export class AccountCrud {
  constructor(
    @InjectRepository(Account)
    private accountRepository: Repository<Account>,
  ) {}

  async createOne(data: Omit<Partial<Account>, 'id'>): Promise<Account> {
    const usersCreated: Account = this.accountRepository.create(data);
    return await this.accountRepository.save(usersCreated);
  }

  async readMany(
    where?: Partial<Account>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof Account)[],
    relations?: string[],
  ): Promise<Account[]> {
    const findOptions: FindManyOptions<Account> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: Account[] = await this.accountRepository.find(findOptions);
    return userFound;
  }

  async updateOne(
    dataToFind: Partial<Account>,
    dataToUpdate: Omit<Partial<Account>, 'id'>,
  ) {
    const accountToUpdate: Account = await this.accountRepository.findOneBy({
      ...dataToFind,
    } as any);
    Object.assign(accountToUpdate, { ...dataToUpdate });
    return await this.accountRepository.save(accountToUpdate);
  }
}
