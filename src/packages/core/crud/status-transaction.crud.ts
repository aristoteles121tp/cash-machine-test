import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { StatusTransaction } from '../../db/postgres/entities';

@Injectable()
export class StatusTransactionCrud {
  constructor(
    @InjectRepository(StatusTransaction)
    private statusTransactionRepository: Repository<StatusTransaction>,
  ) {}

  async createOne(
    data: Omit<Partial<StatusTransaction>, 'id'>,
  ): Promise<StatusTransaction> {
    const transactionCreated: StatusTransaction =
      this.statusTransactionRepository.create(data);
    return await this.statusTransactionRepository.save(transactionCreated);
  }

  async readMany(
    where?: Partial<StatusTransaction>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof StatusTransaction)[],
    relations?: string[],
  ): Promise<StatusTransaction[]> {
    const findOptions: FindManyOptions<StatusTransaction> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: StatusTransaction[] =
      await this.statusTransactionRepository.find(findOptions);
    return userFound;
  }
}
