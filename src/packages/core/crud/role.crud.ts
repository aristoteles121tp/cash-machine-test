import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { Role } from '../../db/postgres/entities';

@Injectable()
export class RoleCrud {
  constructor(
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
  ) {}

  createMany(data: Omit<Partial<Role>, 'id'>[]): Promise<Role[]> {
    const roleCreated = this.roleRepository.create(data);
    return this.roleRepository.save(roleCreated);
  }

  async readMany(
    where?: Partial<Role>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof Role)[],
    take?: number,
  ): Promise<Role[]> {
    const findOptions: FindManyOptions<Role> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (take) {
      findOptions.take = take;
    }
    const userFound: Role[] = await this.roleRepository.find(findOptions);
    return userFound;
  }
}
