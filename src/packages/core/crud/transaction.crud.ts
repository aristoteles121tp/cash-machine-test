import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { Transaction } from '../../db/postgres/entities';

@Injectable()
export class TransactionCrud {
  constructor(
    @InjectRepository(Transaction)
    private transactionRepository: Repository<Transaction>,
  ) {}

  async createOne(
    data: Omit<Partial<Transaction>, 'id'>,
  ): Promise<Transaction> {
    const transactionCreated: Transaction =
      this.transactionRepository.create(data);
    return await this.transactionRepository.save(transactionCreated);
  }

  async readMany(
    where?: Partial<Transaction>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof Transaction)[],
    relations?: string[],
  ): Promise<Transaction[]> {
    const findOptions: FindManyOptions<Transaction> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: Transaction[] = await this.transactionRepository.find(
      findOptions,
    );
    return userFound;
  }
}
