import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { AccountType } from '../../db/postgres/entities';

@Injectable()
export class AccountTypeCrud {
  constructor(
    @InjectRepository(AccountType)
    private accountTypeRepository: Repository<AccountType>,
  ) {}

  async createOne(
    data: Omit<Partial<AccountType>, 'id'>,
  ): Promise<AccountType> {
    const usersCreated: AccountType = this.accountTypeRepository.create(data);
    return await this.accountTypeRepository.save(usersCreated);
  }

  async readMany(
    where?: Partial<AccountType>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof AccountType)[],
    relations?: string[],
  ): Promise<AccountType[]> {
    const findOptions: FindManyOptions<AccountType> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: AccountType[] = await this.accountTypeRepository.find(
      findOptions,
    );
    return userFound;
  }
}
