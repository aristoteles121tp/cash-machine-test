import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { Client } from '../../db/postgres/entities';

@Injectable()
export class ClientCrud {
  constructor(
    @InjectRepository(Client)
    private clientRepository: Repository<Client>,
  ) {}

  async createMany(data: Omit<Partial<Client>, 'id'>[]): Promise<Client[]> {
    return await this.clientRepository.save(data as any);
  }

  async readMany(
    where?: Partial<Client>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof Client)[],
    take?: number,
  ): Promise<Client[]> {
    const findOptions: FindManyOptions<Client> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (take) {
      findOptions.take = take;
    }
    const userFound: Client[] = await this.clientRepository.find(findOptions);
    return userFound;
  }
}
