import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { User } from '../../db/postgres/entities';

@Injectable()
export class UserCrud {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async createOne(data: Omit<Partial<User>, 'id'>): Promise<User> {
    const usersCreated: User = this.userRepository.create(data);
    return await this.userRepository.save(usersCreated);
  }

  async readMany(
    where?: Partial<User>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof User)[],
    relations?: [],
  ): Promise<User[]> {
    const findOptions: FindManyOptions<User> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: User[] = await this.userRepository.find(findOptions);
    return userFound;
  }
}
