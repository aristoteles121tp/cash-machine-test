import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { CardType } from '../../db/postgres/entities';

@Injectable()
export class CardTypeCrud {
  constructor(
    @InjectRepository(CardType)
    private cardTypeRepository: Repository<CardType>,
  ) {}

  async createOne(data: Omit<Partial<CardType>, 'id'>): Promise<CardType> {
    const usersCreated: CardType = this.cardTypeRepository.create(data);
    return await this.cardTypeRepository.save(usersCreated);
  }

  async readMany(
    where?: Partial<CardType>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof CardType)[],
    relations?: string[],
  ): Promise<CardType[]> {
    const findOptions: FindManyOptions<CardType> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: CardType[] = await this.cardTypeRepository.find(
      findOptions,
    );
    return userFound;
  }
}
