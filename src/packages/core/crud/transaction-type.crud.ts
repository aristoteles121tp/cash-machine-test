import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { TransactionType } from '../../db/postgres/entities';

@Injectable()
export class TransactionTypeCrud {
  constructor(
    @InjectRepository(TransactionType)
    private transactionTypeRepository: Repository<TransactionType>,
  ) {}

  async createOne(
    data: Omit<Partial<TransactionType>, 'id'>,
  ): Promise<TransactionType> {
    const transactionCreated: TransactionType =
      this.transactionTypeRepository.create(data);
    return await this.transactionTypeRepository.save(transactionCreated);
  }

  async readMany(
    where?: Partial<TransactionType>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof TransactionType)[],
    relations?: string[],
  ): Promise<TransactionType[]> {
    const findOptions: FindManyOptions<TransactionType> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: TransactionType[] =
      await this.transactionTypeRepository.find(findOptions);
    return userFound;
  }
}
