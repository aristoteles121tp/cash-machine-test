import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { Card } from '../../db/postgres/entities';

@Injectable()
export class CardCrud {
  constructor(
    @InjectRepository(Card)
    private cardRepository: Repository<Card>,
  ) {}

  async createOne(data: Omit<Partial<Card>, 'id'>): Promise<Card> {
    const usersCreated: Card = this.cardRepository.create(data);
    return await this.cardRepository.save(usersCreated);
  }

  async readMany(
    where?: Partial<Card>,
    order?: { [keyof: string]: 'DESC' | 'ASC' },
    select?: (keyof Card)[],
    relations?: string[],
  ): Promise<Card[]> {
    const findOptions: FindManyOptions<Card> = {};
    if (where) {
      findOptions.where = { ...where } as any;
    }
    if (order) {
      findOptions.order = order;
    }
    if (select) {
      findOptions.select = select;
    }
    if (relations) {
      findOptions.relations = relations;
    }
    const userFound: Card[] = await this.cardRepository.find(findOptions);
    return userFound;
  }
}
