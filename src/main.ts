import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setApiSetUp } from './packages';
import { Logger, ValidationPipe } from '@nestjs/common';

const port = process.env.PORT || 3000;
const logger = new Logger('bootstrap');

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['log', 'debug', 'error', 'warn'],
  });
  app.useGlobalPipes(new ValidationPipe());
  setApiSetUp(app, logger);
  await app.listen(port);
  logger.log(`API ready in port: ${process.env.PORT}`);
}
bootstrap();
