import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  ClientSrviceModule,
  AccountServiceModule,
} from './packages/api/controllers';
import { RepositoryModule } from './packages/db';
import { getTypeOrmOptions } from './packages/db/config';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRoot(getTypeOrmOptions()),
    RepositoryModule,
    ClientSrviceModule,
    AccountServiceModule,
  ],
})
export class AppModule {}
